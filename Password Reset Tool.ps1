﻿# By: Landon Lengyel
# Last Updated: May 14th, 2019
#
#
# Note: script will fail if user is not allowed to change their password, or their password is never set to expire


clear

Write-Host "---Welcome to the password reset tool---"


$username = Read-Host "Username"

$user = Get-ADUser -Filter "SAMAccountName -like '$username*'"

# test if script found user
if ($user -ne $null){
    $password = "Password12345"
    $successStatus = $true

    # generate a random special character using ASCII and add it to the end of the $password
    $random = (35..38) + 33 + (42..43) + 47 + (58..64) + 94 | Get-Random -Count 1 | %{ [char]$_ }
    $password = $password + $random


  

    # Change user password to $password
    try { Set-ADAccountPassword -Identity "$username" -NewPassword (ConvertTo-SecureString -AsPlainText "$password" -Force) }
    catch {
        $successStatus = $false
    }
    

    # Set user to change password at next logon
    try { Set-ADUser -Identity "$username" -ChangePasswordAtLogon $TRUE }
    catch {
        $successStatus = $false
    }


    # Let user know password, or inform of failure
    if ($successStatus -eq $true){
        # using multiple colors on one line
        Write-Host "Password was successfully changed to: " -NoNewline; Write-Host $password -ForegroundColor Yellow

        # press just about any key to continue
        [void](Read-Host "Press Enter to continue")

    }
    else {
        Write-Host "The script failed for an unknown reason" -ForegroundColor Red

        # press just about any key to continue
        [void](Read-Host "Press Enter to continue")
        exit
    }


}
else{
    Write-Host "Script was unable to find user: $username"
}